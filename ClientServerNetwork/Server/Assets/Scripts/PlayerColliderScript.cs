﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerColliderScript : MonoBehaviour
{
    public long clientID;
    private ServerNetwork serverNet;
    private ExampleServer serverScript;

    private void Start()
    {
        serverNet = FindObjectOfType<ServerNetwork>();
        serverScript = FindObjectOfType<ExampleServer>();
    }


    private void OnCollisionEnter(Collision hit)
    {
        if(hit.gameObject.GetComponent<WrenchColliderScript>())
            if(hit.gameObject.GetComponent<WrenchColliderScript>().teamId != (int)ExampleServer.instance.GetPlayer(clientID).team && hit.gameObject.GetComponent<WrenchColliderScript>().teamId != (int)ExampleServer.Player.Team.None)
            {
                ExampleServer.instance.GetPlayer(clientID).isOut = true;
            }
            else
            {
                if(hit.gameObject.GetComponent<WrenchColliderScript>().teamId == -1)
                {
                    //assign a team to the wrench
                    hit.gameObject.GetComponent<WrenchColliderScript>().teamId = (int)ExampleServer.instance.GetPlayer(clientID).team;
                    serverNet.CallRPC("PickUpBall", UCNetwork.MessageReceiver.AllClients, hit.gameObject.GetComponent<WrenchColliderScript>().clientId, serverScript.GetPlayer(clientID).playerObject.networkId);
                }
            }
              
    }


}
