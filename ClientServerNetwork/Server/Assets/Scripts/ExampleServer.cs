﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using LitJson;

public class ExampleServer : MonoBehaviour
{
    public static ExampleServer instance;

    public ServerNetwork serverNet;

    Dictionary<int, ServerNetwork.NetworkObject> objs;

    public int portNumber = 603;

    float hitDistance;

    [SerializeField] private GameObject playerPhysicsObject;

    [SerializeField] private GameObject ballPhysicsObject;

    // Stores a player
    [Serializable]
    public class Player
    {
        public long clientId;
        public string playerName;
        public bool isReady;
        public bool isConnected;
        public Team team;
        public bool isPartyLeader = false;
        public bool isOut = false;
        public ServerNetwork.NetworkObject playerObject;
        public GameObject playerCollider;

        public enum Team
        {
            None = -1,
            Red = 0,
            Blue = 1
        }
    }
    [Serializable]
    public class Ball
    {

        public Ball(ServerNetwork.NetworkObject aObject, GameObject obj, Player.Team aTeam)
        {
            ballObject = aObject;
            ballColliderObject = obj;
            ownersTeam = aTeam;
        }
        public ServerNetwork.NetworkObject ballObject;
        public GameObject ballColliderObject;
        public Player.Team ownersTeam;
    }

    [SerializeField] private List<Ball> balls = new List<Ball>();

    [SerializeField] private List<Player> players = new List<Player>();

    [SerializeField] private bool gameInProgress = false;


    public Player GetPlayer(long aClientID)
    {
        foreach (Player p in players)
        {
            if (p.clientId == aClientID)
                return p;
        }
        return null;
    }

    // Use this for initialization
    void Awake()
    {
        instance = this;

        // Initialization of the server network
        ServerNetwork.port = portNumber;
        if (serverNet == null)
        {
            serverNet = GetComponent<ServerNetwork>();
        }
        if (serverNet == null)
        {
            serverNet = (ServerNetwork)gameObject.AddComponent(typeof(ServerNetwork));
            Debug.Log("ServerNetwork component added.");
        }

        //serverNet.EnableLogging("rpcLog.txt");
    }
    private void FixedUpdate()
    {
        foreach (Player p in players)
        {
            if (p.isOut)
                continue;
            p.playerCollider.transform.position = p.playerObject.position;
            p.playerCollider.transform.rotation = p.playerObject.rotation;

        }

        foreach (Ball b in balls)
        {
            b.ballColliderObject.transform.position = b.ballObject.position;
            b.ballColliderObject.transform.rotation = b.ballObject.rotation;
        }
    }

    public void GetPlayerOut(long aClientID)
    {

    }

    public void ThrowABall()
    {
        //Player ask to throw ball, throw it

    }

    // A client has just requested to connect to the server
    void ConnectionRequest(ServerNetwork.ConnectionRequestInfo data)
    {
        Debug.Log("Connection request from " + data.username);

        // We either need to approve a connection or deny it
        if (players.Count <= 16 && !gameInProgress)
        {
            Player newPlayer = new Player();
            newPlayer.clientId = data.id;
            newPlayer.playerName = data.username;
            newPlayer.isConnected = false;
            newPlayer.isReady = false;
            newPlayer.team = (Player.Team)(players.Count % 2);
            players.Add(newPlayer);

            serverNet.ConnectionApproved(data.id);
        }
        /*
        else
        {
            serverNet.ConnectionDenied(data.id);
        }
        */
    }

    void OnClientConnected(long aClientId)
    {
        // Set the isConnected to true on the player
        foreach (Player p in players)
        {
            //Let the party leader start the game
            if (players.Count == 1)
            {
                serverNet.CallRPC("SetReadyButton", aClientId, -1);
                p.isPartyLeader = true;
            }

            if (p.clientId == aClientId)
            {
                p.isConnected = true;
                //Let everyone know that this player joined
                serverNet.CallRPC("SpawnPlayerEntry", UCNetwork.MessageReceiver.AllClients, -1, p.playerName, (int)p.team);
                //Confirm the players name by sending it back to them (we would check if it's innapropriate or if it's empty, if it is either, we'd change it)
                serverNet.CallRPC("SetLocalClientName", aClientId, -1, p.playerName, (int)p.team);
            }
            else if (p.isConnected)
            {
                //Let the connecting player know of everyone else
                serverNet.CallRPC("SpawnPlayerEntry", aClientId, -1, p.playerName, (int)p.team);
            }
        }

        /*
        serverNet.CallRPC("RPCTest", UCNetwork.MessageReceiver.AllClients, -1, 45);
        ServerNetwork.ClientData data = serverNet.GetClientData(serverNet.SendingClientId);
        serverNet.CallRPC("NewClientConnected", UCNetwork.MessageReceiver.AllClients, -1, aClientId, "bob");
        */
    }

    public void StartGame()
    {
        if (GetPlayer(serverNet.SendingClientId).isPartyLeader)
        {
            serverNet.CallRPC("ResetPlayerEntries", UCNetwork.MessageReceiver.AllClients, -1);
            //switch to game scene
            //spawn in balls/players
            //set ui up
            Debug.Log("JUSTIN START THE GAME HERE, DONT YOU SAY SORRY I SEAR TO GOD");
            //This is assuming that the first networked object a player owns in the actual player object
            foreach (Player p in players)
            {
                p.playerObject = serverNet.GetPlayerObject(p.clientId);
                p.playerCollider = Instantiate(playerPhysicsObject, p.playerObject.position, p.playerObject.rotation);
                p.playerCollider.GetComponent<PlayerColliderScript>().clientID = p.clientId;
                if (p.team == Player.Team.Blue)
                    p.playerCollider.GetComponent<MeshRenderer>().material.color = Color.blue;
                else
                    p.playerCollider.GetComponent<MeshRenderer>().material.color = Color.red;
            }
            gameInProgress = true;
            SpawnBalls();
        }

    }
    //JUSTIN CALL THIS WHEN ALL PLAYERS ON ONE TEAM ARE "OUT"
    private void EndGame(int aWinningTeam, float aTimeTilRestart)
    {
        foreach (Player p in players)
        {
            if ((int)p.team == aWinningTeam)
            {
                serverNet.CallRPC("ShowEndScreen", p.clientId, -1, true);
            }
            else
            {
                serverNet.CallRPC("ShowEndScreen", p.clientId, -1, false);
            }
        }

        StartCoroutine(ReturnToLobby(aTimeTilRestart));

    }

    private IEnumerator ReturnToLobby(float aTimeTilRestart)
    {
        yield return new WaitForSeconds(aTimeTilRestart);
        serverNet.CallRPC("HideEndScreen", UCNetwork.MessageReceiver.AllClients, -1);
        serverNet.CallRPC("ResetPlayerEntries", UCNetwork.MessageReceiver.AllClients, -1);
        foreach (Player p in players)
        {
            if (p.isPartyLeader)
                serverNet.CallRPC("SetReadyButton", p.clientId, -1);
            serverNet.CallRPC("SpawnPlayerEntry", UCNetwork.MessageReceiver.AllClients, -1, p.playerName, (int)p.team);
            //restore player bodies
        }

    }


    void OnClientDisconnected(long aClientId)
    {
        // Set the isConnected to true on the player
        foreach (Player p in players)
        {
            if (p.clientId == aClientId)
            {
                p.isConnected = false;
                p.isReady = false;
                serverNet.CallRPC("DestroyPlayerEntry", UCNetwork.MessageReceiver.AllClients, -1, p.playerName);
            }
        }
    }

    private void SpawnBalls()
    {
        ServerNetwork.NetworkObject obj = serverNet.InstantiateNetworkObject("Ball", new Vector3(-2, 1, 3), Quaternion.identity, serverNet.SendingClientId, "");
        serverNet.AddObjectToArea(obj.networkId, 1);
        GameObject ballObject = Instantiate(ballPhysicsObject, obj.position, obj.rotation);
        balls.Add(new Ball(obj, ballObject, Player.Team.None));
        ballObject.GetComponent<WrenchColliderScript>().clientId = obj.networkId;

        obj = serverNet.InstantiateNetworkObject("Ball", new Vector3(-2, 1, 0), Quaternion.identity, serverNet.SendingClientId, "");
        serverNet.AddObjectToArea(obj.networkId, 1);
        ballObject = Instantiate(ballPhysicsObject, obj.position, obj.rotation);
        balls.Add(new Ball(obj, ballObject, Player.Team.None));
        ballObject.GetComponent<WrenchColliderScript>().clientId = obj.networkId;

        obj = serverNet.InstantiateNetworkObject("Ball", new Vector3(-2, 1, -3), Quaternion.identity, serverNet.SendingClientId, "");
        serverNet.AddObjectToArea(obj.networkId, 1);
        ballObject = Instantiate(ballPhysicsObject, obj.position, obj.rotation);
        balls.Add(new Ball(obj, ballObject, Player.Team.None));
        ballObject.GetComponent<WrenchColliderScript>().clientId = obj.networkId;

        obj = serverNet.InstantiateNetworkObject("Ball", new Vector3(2, 1, 3), Quaternion.identity, serverNet.SendingClientId, "");
        serverNet.AddObjectToArea(obj.networkId, 1);
        ballObject = Instantiate(ballPhysicsObject, obj.position, obj.rotation);
        balls.Add(new Ball(obj, ballObject, Player.Team.None));
        ballObject.GetComponent<WrenchColliderScript>().clientId = obj.networkId;

        obj = serverNet.InstantiateNetworkObject("Ball", new Vector3(2, 1, 0), Quaternion.identity, serverNet.SendingClientId, "");
        serverNet.AddObjectToArea(obj.networkId, 1);
        ballObject = Instantiate(ballPhysicsObject, obj.position, obj.rotation);
        balls.Add(new Ball(obj, ballObject, Player.Team.None));
        ballObject.GetComponent<WrenchColliderScript>().clientId = obj.networkId;

        obj = serverNet.InstantiateNetworkObject("Ball", new Vector3(2, 1, -3), Quaternion.identity, serverNet.SendingClientId, "");
        serverNet.AddObjectToArea(obj.networkId, 1);
        balls.Add(new Ball(obj, ballObject, Player.Team.None));
        ballObject.GetComponent<WrenchColliderScript>().clientId = obj.networkId;
    }
}
