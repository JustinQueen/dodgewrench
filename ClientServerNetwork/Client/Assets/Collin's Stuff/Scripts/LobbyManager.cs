﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class LobbyManager : MonoBehaviour
{



    public static LobbyManager Instance;

    [SerializeField] private Transform playerEntryContent;

    [SerializeField] private GameObject playerEntryPrefab;

    [SerializeField] private TextMeshProUGUI playerNameText;

    [SerializeField] private GameObject startGameButton;

    [SerializeField] private GameObject endingGameText;

    private void Start()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
    }
    private void Awake()
    {
        startGameButton.SetActive(false);
        endingGameText.SetActive(false);
    }

    public void SpawnPlayerEntry(string aName, int aTeam)
    {
        GameObject entry = Instantiate(playerEntryPrefab, playerEntryContent);
        entry.GetComponentInChildren<TextMeshProUGUI>().text = aName;
        if (aTeam == 0)
            entry.GetComponent<Image>().color = Color.red;
        else
            entry.GetComponent<Image>().color = Color.blue;

    }
    public void DestroyPlayerEntry(string aName)
    {
        foreach (Transform t in playerEntryContent)
        {
            if(t.gameObject.GetComponentInChildren<TextMeshProUGUI>().text == aName)
            {
                Destroy(t.gameObject);
            }
        }
    }

    public void SetLocalClientName(string aName, int aTeam)
    {
        playerNameText.text = aName;
        if (aTeam == 0)
            playerNameText.color = Color.red;
        else
            playerNameText.color = Color.blue;
    }

    public void SetReadyButton()
    {
        startGameButton.SetActive(true);
        startGameButton.GetComponent<Button>().onClick.AddListener(() => SendStartGameRequest());
    }

    public void SendStartGameRequest()
    {
        ExampleClient.GetInstance().clientNet.CallRPC("StartGame", UCNetwork.MessageReceiver.ServerOnly, -1);
    }

    public void ShowEndScreen(bool aWonGame)
    {
        endingGameText.SetActive(true);
        if (aWonGame)
        {
            endingGameText.GetComponentInChildren<TextMeshProUGUI>().text = "Victory!";
            endingGameText.GetComponentInChildren<TextMeshProUGUI>().color = Color.green;
        }
        else
        {
            endingGameText.GetComponentInChildren<TextMeshProUGUI>().text = "Defeat!";
            endingGameText.GetComponentInChildren<TextMeshProUGUI>().color = Color.red;
        }
    }

    public void HideEndScreen()
    {
        endingGameText.SetActive(false);
    }

    public void ResetPlayerEntries()
    {
        foreach (Transform t in playerEntryContent)
        {
            Destroy(t.gameObject);
        }
    }
}
