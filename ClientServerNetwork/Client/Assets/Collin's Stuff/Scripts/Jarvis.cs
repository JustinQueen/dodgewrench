﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.SceneManagement;


public static class Jarvis
{
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    static void Hello()
    {
        if (System.Environment.UserName.Contains("collin") || System.Environment.UserName.Contains("cdesm"))
        {
            Debug.Log("Hello Collin.");
        }
        else if (System.Environment.UserName.Contains("josh")|| System.Environment.UserName.Contains("joshua"))
        {
            Debug.Log("Hello Josh.");
        }
        else if (System.Environment.UserName.Contains("justin"))
        {
            Debug.Log("Hello Justin.");
        }
        else if (System.Environment.UserName.Contains("chris") || System.Environment.UserName.Contains("christopher"))
        {
            Debug.Log("Hello Chris.");
        }
        else 
        {
            Debug.LogWarning("Who are you?");
        }

    }
}
#endif