﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallScript : MonoBehaviour
{
    private GameObject followThisPlayer;
    private bool isFollowing = false;

    public void PickUpBall(int clientID)
    {
        NetworkSync[] networkedObjects = FindObjectsOfType<NetworkSync>();

        foreach(NetworkSync obj in networkedObjects)
        {
            if(obj.GetId() == clientID)
            {
                followThisPlayer = obj.gameObject;
                isFollowing = true;
            }
        }
    }

    private void Update()
    {
        if(isFollowing)
        {
            Vector3 playerPos = followThisPlayer.transform.position;
            playerPos.y += 1;
            transform.position = playerPos;
        }
    }
}
